package com.project.hcmuaf.foodordering.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.project.hcmuaf.foodordering.R;
import com.project.hcmuaf.foodordering.controller.UserController;
import com.project.hcmuaf.foodordering.entity.User;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, FirebaseAuth.AuthStateListener {

    public static final String USER_LOGIN_ID = "UserLogin";
    public static final String HO_TEN_USER_LOGIN = "ho_ten_user_login";
    public static final String EMAIL_USER_LOGIN = "email_user_login";
    public static final String SDT_USER_LOGIN = "sdt_user_login";
    public static final String TYPE_USER_LOGIN = "type_user_login";
    public static final String U_ID = "uId";

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseNodeThanhVien;

    private Button btnDangNhap, btnDangNhapGoogle;
    private LoginButton btnDangNhapFacebook;
    private TextView txtDangKy, txtQuenMatKhau;
    private EditText edEmailDN, edPasswordDN;
    private ProgressDialog progressDialog;
    private SharedPreferences sharedPreferences;

    private UserController userController;

    public static int TYPE_LOGIN = 0;

    private GoogleApiClient apiClient;
    public static int CODE_DANG_NHAP_GOOGLE = 99;

    private CallbackManager mcallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.layout_login);

        databaseNodeThanhVien = FirebaseDatabase.getInstance().getReference().child("thanhviens");

        edEmailDN = findViewById(R.id.edEmailDN);
        edPasswordDN = findViewById(R.id.edPasswordDN);
        btnDangNhap = findViewById(R.id.btnDangNhap);
        btnDangNhap.setOnClickListener(this);
        progressDialog = new ProgressDialog(this);

        txtDangKy = findViewById(R.id.txtDangKy);
        txtDangKy.setOnClickListener(this);
        txtQuenMatKhau = findViewById(R.id.txtQuenMatKhau);
        txtQuenMatKhau.setOnClickListener(this);

        // get firebase
        firebaseAuth = FirebaseAuth.getInstance();

        // seting dang nhap google
        btnDangNhapGoogle = findViewById(R.id.btnDangNhapGoogle);
        btnDangNhapGoogle.setOnClickListener(this);
        getClientGoogle();

        // setting dang nhap facebook
        mcallbackManager = CallbackManager.Factory.create();
        btnDangNhapFacebook = findViewById(R.id.btnDangNhapFacebook);
        btnDangNhapFacebook.setReadPermissions("email", "public_profile");
        btnDangNhapFacebook.registerCallback(mcallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                TYPE_LOGIN = 2;
                String tokenId = loginResult.getAccessToken().getToken();
                chungThucDangNhap(tokenId);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btnDangNhapGoogle:
                dangNhapBangGoogle(apiClient);
                break;
            case R.id.btnDangNhap:
                dangNhapTaiKhoan();
                break;
            case R.id.txtDangKy:
                startActivity(new Intent(this, DangKyActivity.class));
                break;
            case R.id.txtQuenMatKhau:
                startActivity(new Intent(this, QuenMatKhauActivity.class));
                break;
        }
    }

    /**
     *
     */
    private void dangNhapTaiKhoan() {
        progressDialog.setMessage(getString(R.string.dangxuly));
        progressDialog.show();
        String strEmail = edEmailDN.getText().toString().trim();
        String strPassword = edPasswordDN.getText().toString().trim();
        if (strEmail.length() == 0) {
            Toast.makeText(this, "Lỗi email chưa được nhập!", Toast.LENGTH_SHORT).show();
        } else if (strPassword.length() == 0) {
            Toast.makeText(this, "Lỗi password chưa được nhập!", Toast.LENGTH_SHORT).show();
        } else if (strPassword.length() < 6) {
            Toast.makeText(this, "Lỗi password nhập phải lớn hơn 6 ký tự!", Toast.LENGTH_SHORT).show();
        } else {
            firebaseAuth.signInWithEmailAndPassword(strEmail, strPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (!task.isSuccessful()) {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, "Lỗi emai hoặc password không chính xác!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    /**
     * Khoi tao clientGoogle
     */
    private void getClientGoogle() {
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder()
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail().build();

        apiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
    }

    /**
     * Dang nhap bang google
     *
     * @param googleApiClient
     */
    private void dangNhapBangGoogle(GoogleApiClient googleApiClient) {
        TYPE_LOGIN = 1;
        Intent iDNGoogle = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(iDNGoogle, CODE_DANG_NHAP_GOOGLE);
    }

    /**
     * Chung thuc dang nhap
     *
     * @param tokenId
     */
    private void chungThucDangNhap(String tokenId) {
        if (TYPE_LOGIN == 1) {
            AuthCredential authCredential = GoogleAuthProvider.getCredential(tokenId, null);
            firebaseAuth.signInWithCredential(authCredential);
        } else if (TYPE_LOGIN == 2) {
            AuthCredential authCredential = FacebookAuthProvider.getCredential(tokenId);
            firebaseAuth.signInWithCredential(authCredential);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_DANG_NHAP_GOOGLE) {
            if (resultCode == RESULT_OK) {
                GoogleSignInResult signInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                GoogleSignInAccount account = signInResult.getSignInAccount();
                String tokenID = account.getIdToken();
                chungThucDangNhap(tokenID);
            }
        } else {
            mcallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        createUser();
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(this);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        progressDialog.dismiss();
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser != null) {
            setSesionLogin(firebaseAuth.getUid());
            Intent iTrangChu = new Intent(this, TrangChuActivity.class);
            startActivity(iTrangChu);
        }
    }

    private void setSesionLogin(String uid) {
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        User userLogin = snapshot.getValue(User.class);
                        if (uid.equals(userLogin.getUid())) {
                            sharedPreferences = getSharedPreferences(USER_LOGIN_ID, MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(U_ID, uid);
                            editor.putString(HO_TEN_USER_LOGIN, userLogin.getHoten());
                            editor.putString(EMAIL_USER_LOGIN, userLogin.getEmail());
                            editor.putString(SDT_USER_LOGIN, userLogin.getSdt());
                            editor.putString(TYPE_USER_LOGIN, userLogin.getType());
                            editor.commit();
                        }
                    }
                } catch (Exception ex) {
                    Log.e("getDanhSachQuanAn: ", ex.getMessage());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };
        databaseNodeThanhVien.addValueEventListener(valueEventListener);
    }

    private void createUser() {
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        userController = new UserController();
        User user = new User(firebaseUser.getUid(), firebaseUser.getDisplayName(), firebaseUser.getEmail(), firebaseUser.getPhoneNumber(), "user.png", "2");
        userController.dangKyThanhVien(user);
    }
}
