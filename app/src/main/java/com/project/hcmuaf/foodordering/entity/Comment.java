package com.project.hcmuaf.foodordering.entity;

import com.google.firebase.database.Exclude;

import java.io.Serializable;

/**
 * Created by HC on 1/4/2019.
 */

public class Comment implements Serializable {
    private double chamdiem;
    private long luotthich;
    private String mauser;
    private String noidung;
    @Exclude
    private User user;

    public double getChamdiem() {
        return chamdiem;
    }

    public void setChamdiem(double chamdiem) {
        this.chamdiem = chamdiem;
    }

    public long getLuotthich() {
        return luotthich;
    }

    public void setLuotthich(long luotthich) {
        this.luotthich = luotthich;
    }

    public String getMauser() {
        return mauser;
    }

    public void setMauser(String mauser) {
        this.mauser = mauser;
    }

    public String getNoidung() {
        return noidung;
    }

    public void setNoidung(String noidung) {
        this.noidung = noidung;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
