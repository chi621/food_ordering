package com.project.hcmuaf.foodordering.model;

import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.project.hcmuaf.foodordering.entity.Comment;
import com.project.hcmuaf.foodordering.entity.User;

/**
 * Created by HC on 1/6/2019.
 */

public class CommentModel {
    private String KEY_DATABASE = "binhluans";
    private DatabaseReference databaseNodeComment;

    public CommentModel() {
        databaseNodeComment = FirebaseDatabase.getInstance().getReference().child(KEY_DATABASE);
    }

    public void saveComment(String maQuanAn, Comment comment) {
        try {
            databaseNodeComment.child(maQuanAn).push().setValue(comment);
        } catch (Exception ex) {
            Log.e("saveComment: ", ex.getMessage());
        }
    }
}
