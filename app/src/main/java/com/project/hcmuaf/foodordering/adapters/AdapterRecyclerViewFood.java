package com.project.hcmuaf.foodordering.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.project.hcmuaf.foodordering.R;
import com.project.hcmuaf.foodordering.entity.Comment;
import com.project.hcmuaf.foodordering.entity.QuanAn;
import com.project.hcmuaf.foodordering.view.ChiTietQuanAnActivity;
import com.project.hcmuaf.foodordering.view.SlashScreenActivity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by HC on 11/12/2018.
 */
public class AdapterRecyclerViewFood extends RecyclerView.Adapter<AdapterRecyclerViewFood.ViewHolder> {

    public static final String QUAN_AN_CLICK = "quanAnClick";
    private List<QuanAn> quanAns;
    private int resource;
    private long downLoad = 1024 * 1024;
    private DecimalFormat formatter = new DecimalFormat("#0.0");
    private Context context;
    private SharedPreferences sharedPreferences;

    public AdapterRecyclerViewFood(List<QuanAn> quanAns, int resource, Context context) {
        this.quanAns = quanAns;
        this.resource = resource;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layoutBinhluan1, layoutBinhluan2;
        TextView txtTenQuanAn, txtTenBinhLuan1, txtTenBinhLuan2, txtNoiDungBinhLuan1, txtNoiDungBinhLuan2, txtDiemBinhLuan1, txtDiemBinhLuan2, txtTongBinhLuan, txtDiemQuanAn, txtKhoanCach;
        ImageView imgHinhQuanAn;
        CircleImageView imgHinhAnhBinhLuan1, imgHinhAnhBinhLuan2;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTenQuanAn = itemView.findViewById(R.id.txtTenQuanAn);
            imgHinhQuanAn = itemView.findViewById(R.id.imgHinhQuanAn);
            txtDiemQuanAn = itemView.findViewById(R.id.txtDiemQuanAn);

            layoutBinhluan1 = itemView.findViewById(R.id.layoutBinhluan1);
            layoutBinhluan2 = itemView.findViewById(R.id.layoutBinhluan2);
            imgHinhAnhBinhLuan1 = itemView.findViewById(R.id.imgHinhAnhBinhLuan1);
            imgHinhAnhBinhLuan2 = itemView.findViewById(R.id.imgHinhAnhBinhLuan2);
            txtTenBinhLuan1 = itemView.findViewById(R.id.txtTenBinhLuan1);
            txtTenBinhLuan2 = itemView.findViewById(R.id.txtTenBinhLuan2);
            txtNoiDungBinhLuan1 = itemView.findViewById(R.id.txtNoiDungBinhLuan1);
            txtNoiDungBinhLuan2 = itemView.findViewById(R.id.txtNoiDungBinhLuan2);
            txtDiemBinhLuan1 = itemView.findViewById(R.id.txtDiemBinhLuan1);
            txtDiemBinhLuan2 = itemView.findViewById(R.id.txtDiemBinhLuan2);
            txtTongBinhLuan = itemView.findViewById(R.id.txtTongBinhLuan);
            txtKhoanCach = itemView.findViewById(R.id.txtKhoanCach);
        }
    }

    @Override
    public AdapterRecyclerViewFood.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AdapterRecyclerViewFood.ViewHolder holder, int position) {
        QuanAn quanAn = quanAns.get(position);
        // anh xa khoang cach
        double khoangCach = 0;
        if (quanAn.getDiachi() != null) {
            Location locationQuanAn = new Location("");
            locationQuanAn.setLatitude(quanAn.getLatitude());
            locationQuanAn.setLongitude(quanAn.getLongitude());

            khoangCach = getVitriUser().distanceTo(locationQuanAn);
            holder.txtKhoanCach.setText(String.valueOf(formatter.format(khoangCach / 1000)) + "km");
        } else {
            holder.txtKhoanCach.setText("testkm");
        }

        // anh xạ tên quan an
        holder.txtTenQuanAn.setText(quanAn.getTenquanan());
        // anh xạ hình ảnh cho quan an
        if (quanAn.getHinhanhQuanAn().size() > 0) {
            StorageReference storageHinhAnh = FirebaseStorage.getInstance().getReference()
                    .child("hinhanh")
                    .child(quanAn.getHinhanhQuanAn().get(0));
            storageHinhAnh.getBytes(downLoad).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    holder.imgHinhQuanAn.setImageBitmap(bitmap);
                }
            });
        }

        // anh xạ comment và số lượng comment
        if (!quanAn.getLstComments().isEmpty()) {
            holder.layoutBinhluan1.setVisibility(View.VISIBLE);
            Comment commentTemp1 = quanAn.getLstComments().get(0);
            holder.txtTenBinhLuan1.setText(commentTemp1.getUser().getHoten());
            holder.txtNoiDungBinhLuan1.setText(commentTemp1.getNoidung());
            holder.txtDiemBinhLuan1.setText(String.valueOf(commentTemp1.getChamdiem()) + "đ");

            StorageReference avatar = FirebaseStorage.getInstance().getReference()
                    .child("hinhanh")
                    .child(commentTemp1.getUser().getHinhAnh());
            avatar.getBytes(downLoad).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    holder.imgHinhAnhBinhLuan1.setImageBitmap(bitmap);
                }
            });
            if (quanAn.getLstComments().size() >= 2) {
                holder.layoutBinhluan2.setVisibility(View.VISIBLE);
                Comment commentTemp2 = quanAn.getLstComments().get(1);
                holder.txtTenBinhLuan2.setText(commentTemp2.getUser().getHoten());
                holder.txtNoiDungBinhLuan2.setText(commentTemp2.getNoidung());
                holder.txtDiemBinhLuan2.setText(String.valueOf(commentTemp2.getChamdiem()) + "đ");
                avatar = FirebaseStorage.getInstance().getReference()
                        .child("hinhanh")
                        .child(commentTemp2.getUser().getHinhAnh());
                avatar.getBytes(downLoad).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                    @Override
                    public void onSuccess(byte[] bytes) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        holder.imgHinhAnhBinhLuan2.setImageBitmap(bitmap);
                    }
                });
            } else {
                holder.layoutBinhluan2.setVisibility(View.GONE);
            }
            holder.txtTongBinhLuan.setText(String.valueOf(quanAn.getLstComments().size()));
            double dtbQuanAn = 0;
            for (Comment commentTemp : quanAn.getLstComments()) {
                dtbQuanAn += commentTemp.getChamdiem();
            }
            dtbQuanAn = dtbQuanAn / quanAn.getLstComments().size();
            holder.txtDiemQuanAn.setText(String.valueOf(formatter.format(dtbQuanAn)) + "đ");
        } else {
            holder.layoutBinhluan1.setVisibility(View.GONE);
            holder.layoutBinhluan2.setVisibility(View.GONE);
            holder.txtTongBinhLuan.setText("0");
            holder.txtDiemQuanAn.setText("0.0đ");
        }

        holder.txtTenQuanAn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent iChiTietQuanAn = new Intent(context, ChiTietQuanAnActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(QUAN_AN_CLICK, quanAn);
                iChiTietQuanAn.putExtras(bundle);
                context.startActivity(iChiTietQuanAn);
            }
        });
    }

    @Override
    public int getItemCount() {
        return quanAns.size();
    }

    private Location getVitriUser() {
        sharedPreferences = context.getSharedPreferences(SlashScreenActivity.TOA_DO_USER, Context.MODE_PRIVATE);
        Location locationVitriUser = new Location("");
        locationVitriUser.setLatitude(Double.parseDouble(sharedPreferences.getString("latitude", "0")));
        locationVitriUser.setLongitude(Double.parseDouble(sharedPreferences.getString("longitude", "0")));
        return locationVitriUser;
    }

}
