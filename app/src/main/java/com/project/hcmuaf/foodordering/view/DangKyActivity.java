package com.project.hcmuaf.foodordering.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.project.hcmuaf.foodordering.R;
import com.project.hcmuaf.foodordering.controller.UserController;
import com.project.hcmuaf.foodordering.entity.User;

public class DangKyActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnDangKy;
    private EditText edHoTen, edSdt, edEmailDN, edPasswordDN, edNhapLaiPassword;
    private String strHoten, strSdt, strEmailDN, strPasswordDN, strNhapLaiPassword;

    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;

    private UserController userController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dangky);

        firebaseAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);
        btnDangKy = findViewById(R.id.btnDangKy);
        btnDangKy.setOnClickListener(this);
        edHoTen = findViewById(R.id.edHoTen);
        edSdt = findViewById(R.id.edSdt);
        edEmailDN = findViewById(R.id.edEmailDN);
        edPasswordDN = findViewById(R.id.edPasswordDN);
        edNhapLaiPassword = findViewById(R.id.edNhapLaiPassword);
    }

    @Override
    public void onClick(View view) {
        progressDialog.setMessage(getString(R.string.dangxuly));
        progressDialog.show();

        strHoten = edHoTen.getText().toString();
        strSdt = edSdt.getText().toString();
        strEmailDN = edEmailDN.getText().toString();
        strPasswordDN = edPasswordDN.getText().toString();
        strNhapLaiPassword = edNhapLaiPassword.getText().toString();

        String loiDangKy = validate();
        if (!"".equals(loiDangKy)) {
            progressDialog.dismiss();
            Toast.makeText(this, loiDangKy, Toast.LENGTH_SHORT).show();
        } else {
            firebaseAuth.createUserWithEmailAndPassword(strEmailDN, strPasswordDN).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        progressDialog.dismiss();
                        User user = new User(task.getResult().getUser().getUid(), strHoten, strEmailDN, strSdt, "user.png", "1");
                        userController = new UserController();
                        userController.dangKyThanhVien(user);
                        startActivity(new Intent(DangKyActivity.this, LoginActivity.class));
                        Toast.makeText(DangKyActivity.this, getString(R.string.dangkyCong).toString(), Toast.LENGTH_SHORT).show();
                    } else {
                        progressDialog.dismiss();
                        if ("ERROR_EMAIL_ALREADY_IN_USE".equals(((FirebaseAuthException) task.getException()).getErrorCode())) {
                            Toast.makeText(DangKyActivity.this, getString(R.string.loiEmailFirebase).toString(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(DangKyActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            });

        }
    }

    /**
     * @return
     */
    private String validate() {
        StringBuilder loiThongBao = new StringBuilder();
        StringBuilder strRtn = new StringBuilder();
        if (strHoten.trim().length() == 0) {
            loiThongBao.append("họ tên, ");
        }
        if (strSdt.trim().length() == 0) {
            loiThongBao.append("SĐT, ");
        }
        if (strEmailDN.trim().length() == 0) {
            loiThongBao.append("email, ");
        }
        if (strPasswordDN.trim().length() == 0) {
            loiThongBao.append("password, ");
        }
        if (strNhapLaiPassword.trim().length() == 0) {
            loiThongBao.append("nhập lại password ");
        }

        if (!"".equals(loiThongBao) && strNhapLaiPassword.trim().length() > 0
                && strPasswordDN.trim().length() > 0
                && !strPasswordDN.equals(strNhapLaiPassword)) {
            loiThongBao.append(getString(R.string.loiMatKhau));
        }
        return "".equals(loiThongBao.toString()) ? strRtn.toString()
                : strRtn.append("Lỗi ").append(loiThongBao)
                .append(getString(R.string.loiKhongNHap)).toString();
    }
}
