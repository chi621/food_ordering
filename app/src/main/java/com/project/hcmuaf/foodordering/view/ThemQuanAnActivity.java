package com.project.hcmuaf.foodordering.view;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

import com.project.hcmuaf.foodordering.R;

import java.util.Calendar;

public class ThemQuanAnActivity extends AppCompatActivity implements View.OnClickListener {

    private String gioMoCua;
    private String gioDongCua;
    private String khuvuc;

    Button btnGioMoCua, btnGioDongCua, btnThemQuanAn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_them_quan_an);

        btnGioDongCua = findViewById(R.id.btnGioDongCua);
        btnGioMoCua = findViewById(R.id.btnGioMoCua);
    }

    @Override
    public void onClick(View v) {
        Calendar calendar = Calendar.getInstance();
        int gio = calendar.get(Calendar.HOUR_OF_DAY);
        int phut = calendar.get(Calendar.MINUTE);

        switch (v.getId()) {
            case R.id.btnGioDongCua:
                TimePickerDialog timePickerDialog = new TimePickerDialog(ThemQuanAnActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        gioDongCua = hourOfDay + ":" + minute;
                        ((Button) v).setText(gioDongCua);
                    }
                }, gio, phut, true);
                timePickerDialog.show();
                break;

            case R.id.btnGioMoCua:
                TimePickerDialog moCuaTimePickerDialog = new TimePickerDialog(ThemQuanAnActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        gioMoCua = hourOfDay + ":" + minute;
                        ((Button) v).setText(gioMoCua);
                    }
                }, gio, phut, true);
                moCuaTimePickerDialog.show();
                break;
        }
    }
}
