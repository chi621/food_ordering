package com.project.hcmuaf.foodordering.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.project.hcmuaf.foodordering.R;
import com.project.hcmuaf.foodordering.controller.FoodController;

/**
 * Created by HC on 11/11/2018.
 */

public class FoodFragment extends android.support.v4.app.Fragment{
    private FoodController foodController;
    private RecyclerView recyclerView;
    private ProgressBar loadFood;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_food, container, false);
        recyclerView = view.findViewById(R.id.rcvFood);
        loadFood = view.findViewById(R.id.loadFood);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        foodController = new FoodController(getContext());
        foodController.getDanhSachQuanAnForRecyclerView(recyclerView, loadFood);
    }
}
