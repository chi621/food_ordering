package com.project.hcmuaf.foodordering.view;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.project.hcmuaf.foodordering.R;

public class QuenMatKhauActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth firebaseAuth;

    private EditText edEmailKhoiPhucMatKhau;
    private Button btnKhoiPhucMatKhau;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_quenmatkhau);

        firebaseAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);
        edEmailKhoiPhucMatKhau = findViewById(R.id.edEmailKhoiPhucMatKhau);
        btnKhoiPhucMatKhau = findViewById(R.id.btnKhoiPhucMatKhau);
        btnKhoiPhucMatKhau.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        progressDialog.setMessage(getString(R.string.dangxuly));
        progressDialog.show();

        String edEmai = edEmailKhoiPhucMatKhau.getText().toString().trim();
        if (edEmai.length() == 0) {
            Toast.makeText(this, "Lỗi email chưa được nhập!", Toast.LENGTH_SHORT).show();
        } else if (edEmai.length() > 0 && !Patterns.EMAIL_ADDRESS.matcher(edEmai).matches()) {
            Toast.makeText(this, "Lỗi email không hợp lệ!", Toast.LENGTH_SHORT).show();
        } else {
            firebaseAuth.sendPasswordResetEmail(edEmai).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        progressDialog.dismiss();
                        Toast.makeText(QuenMatKhauActivity.this, "Vui lòng kiêm tra email để tạo lại mật khẩu!", Toast.LENGTH_SHORT).show();
                    } else {
                        if ("ERROR_USER_NOT_FOUND".equals(((FirebaseAuthException)task.getException()).getErrorCode())) {
                            progressDialog.dismiss();
                            Toast.makeText(QuenMatKhauActivity.this, "Email chưa được đăng ký vui lòng thử lại!", Toast.LENGTH_SHORT).show();
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(QuenMatKhauActivity.this, "Gửi email không thành công vui lòng thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }
}
