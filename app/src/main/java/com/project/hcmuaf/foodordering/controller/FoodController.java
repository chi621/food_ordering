package com.project.hcmuaf.foodordering.controller;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.project.hcmuaf.foodordering.R;
import com.project.hcmuaf.foodordering.adapters.AdapterRecyclerViewFood;
import com.project.hcmuaf.foodordering.database.QuanAnDatabase;
import com.project.hcmuaf.foodordering.entity.QuanAn;
import com.project.hcmuaf.foodordering.model.QuanAnModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HC on 11/12/2018.
 */

public class FoodController {
    private Context context;
    private QuanAnModel quanAnModel;
    private AdapterRecyclerViewFood adapterRecyclerViewFood;

    public FoodController(Context context) {
        this.context = context;
        this.quanAnModel = new QuanAnModel();
    }

    public void getDanhSachQuanAnForRecyclerView(RecyclerView recyclerView, ProgressBar loadFood){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

        final List<QuanAn> quanAnList = new ArrayList<>();
        adapterRecyclerViewFood = new AdapterRecyclerViewFood(quanAnList, R.layout.customer_layout_recycleview_food, context);
        recyclerView.setAdapter(adapterRecyclerViewFood);

        QuanAnDatabase quanAnDatabase = new QuanAnDatabase() {
            @Override
            public void getDanhSachQuanAn(QuanAn quanAn) {
                quanAnList.add(quanAn);
                adapterRecyclerViewFood.notifyDataSetChanged();
                loadFood.setVisibility(View.GONE);
            }
        };
        quanAnModel.getDanhSachQuanAn(quanAnDatabase);
    }
}
