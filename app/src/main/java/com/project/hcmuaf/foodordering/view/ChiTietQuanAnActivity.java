package com.project.hcmuaf.foodordering.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.project.hcmuaf.foodordering.R;
import com.project.hcmuaf.foodordering.adapters.AdapterBinhLuan;
import com.project.hcmuaf.foodordering.adapters.AdapterRecyclerViewFood;
import com.project.hcmuaf.foodordering.entity.Comment;
import com.project.hcmuaf.foodordering.entity.QuanAn;
import com.project.hcmuaf.foodordering.model.UserModel;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ChiTietQuanAnActivity extends AppCompatActivity implements View.OnClickListener, ValueEventListener {

    private DecimalFormat formatter = new DecimalFormat("#0.0");
    private QuanAn quanAn;
    private Toolbar toolbar;
    private ImageView imHinhAnhQuanAn;
    private TextView txtTieuDeToolbar, txtTenQuanAn, txtDiaChiQuanAn, txtThoiGianHoatDong, txtDiemDanhGia, tongSoBinhLuan, txtTrangThaiHoatDong;
    private Button btnBinhLuan;
    private RecyclerView recyclerBinhLuanQuanAn;
    private UserModel userModel = new UserModel();

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReferenceBinhLuan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_chi_tiet_quan_an);

        quanAn = (QuanAn) getIntent().getExtras().getSerializable(AdapterRecyclerViewFood.QUAN_AN_CLICK);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReferenceBinhLuan = firebaseDatabase.getReference().child("binhluans").child(quanAn.getMaquanan());
        databaseReferenceBinhLuan.addValueEventListener(this);

        imHinhAnhQuanAn = (ImageView) findViewById(R.id.imHinhQuanAn);
        toolbar = findViewById(R.id.toolbarChiTietQuanAn);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtTieuDeToolbar = findViewById(R.id.txtTieuDeToolbar);

        txtTenQuanAn = findViewById(R.id.txtTenQuanAn);
        txtDiaChiQuanAn = findViewById(R.id.txtDiaChiQuanAn);
        txtThoiGianHoatDong = findViewById(R.id.txtDiaChiQuanAn);
        txtDiemDanhGia = findViewById(R.id.txtDiemDanhGia);
        tongSoBinhLuan = findViewById(R.id.tongSoBinhLuan);
        txtTrangThaiHoatDong = findViewById(R.id.txtTrangThaiHoatDong);

        StorageReference storageHinhQuanAn = FirebaseStorage.getInstance().getReference().child("hinhanh").child(quanAn.getHinhanhQuanAn().get(0));
        long ONE_MEGABYTE = 1024 * 1024;
        storageHinhQuanAn.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                imHinhAnhQuanAn.setImageBitmap(bitmap);
            }
        });
        recyclerBinhLuanQuanAn = findViewById(R.id.recyclerBinhLuanQuanAn);

        btnBinhLuan = findViewById(R.id.btnBinhLuan);
        btnBinhLuan.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        try {
            txtTieuDeToolbar.setText(quanAn.getTenquanan());
            txtTenQuanAn.setText(quanAn.getTenquanan());
            txtDiaChiQuanAn.setText(quanAn.getDiachi());
            txtThoiGianHoatDong.setText(quanAn.getGiomocua() + " - " + quanAn.getGiodongcua());
            realTime();
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

            String giohientai = dateFormat.format(calendar.getTime());
            String giomocua = quanAn.getGiomocua();
            String giodongcua = quanAn.getGiodongcua();
            Date dateHienTai = dateFormat.parse(giohientai);
            Date dateMoCua = dateFormat.parse(giomocua);
            Date dateDongCua = dateFormat.parse(giodongcua);
            if (dateHienTai.after(dateMoCua) && dateHienTai.before(dateDongCua)) {
                txtTrangThaiHoatDong.setText("Đang mở cửa");
            } else {
                txtTrangThaiHoatDong.setText("Chưa mở cửa");
            }
        } catch (Exception e) {
            Log.e("ChiTietQuanAnActivity", e.getMessage());
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent iChiTietQuanAn = new Intent(this, TrangChuActivity.class);
        startActivity(iChiTietQuanAn);
        return true;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btnBinhLuan:
                Intent iChiTietQuanAn = new Intent(this, BinhLuanActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(AdapterRecyclerViewFood.QUAN_AN_CLICK, quanAn);
                iChiTietQuanAn.putExtras(bundle);
                startActivity(iChiTietQuanAn);
                break;
        }
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        List<Comment> comments = new ArrayList<>();
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            Comment comment = snapshot.getValue(Comment.class);
            comment.setUser(userModel.getUserByUid(comment.getMauser()));
            comments.add(comment);
        }
        quanAn.setLstComments(comments);

        realTime();

        AdapterBinhLuan adapterBinhLuan = new AdapterBinhLuan(this, R.layout.custom_layout_binhluan, quanAn.getLstComments());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerBinhLuanQuanAn.setLayoutManager(layoutManager);
        recyclerBinhLuanQuanAn.setAdapter(adapterBinhLuan);
        adapterBinhLuan.notifyDataSetChanged();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        realTime();
        AdapterBinhLuan adapterBinhLuan = new AdapterBinhLuan(this, R.layout.custom_layout_binhluan, quanAn.getLstComments());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerBinhLuanQuanAn.setLayoutManager(layoutManager);
        recyclerBinhLuanQuanAn.setAdapter(adapterBinhLuan);
        adapterBinhLuan.notifyDataSetChanged();
    }

    private void realTime() {
        double diemDanhGia = 0;
        if(!quanAn.getLstComments().isEmpty()) {
            for (Comment commentTemp : quanAn.getLstComments()) {
                diemDanhGia += commentTemp.getChamdiem();
            }
            diemDanhGia = diemDanhGia / quanAn.getLstComments().size();
            txtDiemDanhGia.setText(String.valueOf(formatter.format(diemDanhGia)) + "đ");
            tongSoBinhLuan.setText(String.valueOf(quanAn.getLstComments().size()));
        } else {
            txtDiemDanhGia.setText("0đ");
            tongSoBinhLuan.setText("0");
        }

    }
}
