package com.project.hcmuaf.foodordering.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.project.hcmuaf.foodordering.R;
import com.project.hcmuaf.foodordering.entity.Comment;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by HC on 1/6/2019.
 */

public class AdapterBinhLuan extends RecyclerView.Adapter<AdapterBinhLuan.ViewHolder> {
    private Context context;
    private int layout;
    private long downLoad = 1024 * 1024;
    private List<Comment> commentList;

    public AdapterBinhLuan(Context context, int layout, List<Comment> commentList) {
        this.context = context;
        this.layout = layout;
        this.commentList = commentList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView cicleImageUser;
        TextView txtHoTenNguoiBinhLuan, txtNodungbinhluan, txtChamDiemBinhLuan;
        public ViewHolder(View itemView) {
            super(itemView);
            cicleImageUser = itemView.findViewById(R.id.cicleImageUser);
            txtHoTenNguoiBinhLuan = itemView.findViewById(R.id.txtHoTenNguoiBinhLuan);
            txtNodungbinhluan = itemView.findViewById(R.id.txtNodungbinhluan);
            txtChamDiemBinhLuan = itemView.findViewById(R.id.txtChamDiemBinhLuan);

        }
    }


    @Override
    public AdapterBinhLuan.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterBinhLuan.ViewHolder holder, int position) {
        if (!commentList.isEmpty()) {
            Comment comment = commentList.get(position);
            holder.txtHoTenNguoiBinhLuan.setText(comment.getUser().getHoten());
            holder.txtNodungbinhluan.setText(comment.getNoidung());
            holder.txtChamDiemBinhLuan.setText(String.valueOf(comment.getChamdiem()));

            StorageReference avatar = FirebaseStorage.getInstance().getReference()
                    .child("hinhanh")
                    .child(comment.getUser().getHinhAnh());
            avatar.getBytes(downLoad).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    holder.cicleImageUser.setImageBitmap(bitmap);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

}
