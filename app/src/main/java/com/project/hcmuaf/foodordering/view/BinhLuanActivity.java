package com.project.hcmuaf.foodordering.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.project.hcmuaf.foodordering.R;
import com.project.hcmuaf.foodordering.adapters.AdapterRecyclerViewFood;
import com.project.hcmuaf.foodordering.controller.CommentController;
import com.project.hcmuaf.foodordering.entity.Comment;
import com.project.hcmuaf.foodordering.entity.QuanAn;

public class BinhLuanActivity extends AppCompatActivity {
    private CommentController commentController;
    private QuanAn quanAn;
    private SharedPreferences sharedPreferences;
    private TextView txtTenQuanAn, txtDiaChiQuanAn, edNoiDungBinhLuan;
    private RadioGroup rdgChamDiem;
    private Button btnDangBinhLuan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_binh_luan);

        quanAn = (QuanAn) getIntent().getExtras().getSerializable(AdapterRecyclerViewFood.QUAN_AN_CLICK);
        sharedPreferences = getSharedPreferences(LoginActivity.USER_LOGIN_ID, Context.MODE_PRIVATE);

        txtTenQuanAn = findViewById(R.id.txtTenQuanAn);
        txtDiaChiQuanAn = findViewById(R.id.txtDiaChiQuanAn);
        edNoiDungBinhLuan = findViewById(R.id.edNoiDungBinhLuan);
        btnDangBinhLuan = findViewById(R.id.btnDangBinhLuan);

        rdgChamDiem = findViewById(R.id.rdgChamDiem);

        btnDangBinhLuan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!validate()) {
                    return;
                } else {
                    Comment comment = new Comment();
                    comment.setMauser(sharedPreferences.getString(LoginActivity.U_ID, ""));
                    comment.setChamdiem(getChamDiem());
                    comment.setNoidung(String.valueOf(edNoiDungBinhLuan.getText()));

                    commentController = new CommentController();
                    commentController.addComment(quanAn.getMaquanan(), comment);

                    Intent iChiTietQuanAn = new Intent(BinhLuanActivity.this, ChiTietQuanAnActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AdapterRecyclerViewFood.QUAN_AN_CLICK, quanAn);
                    iChiTietQuanAn.putExtras(bundle);
                    startActivity(iChiTietQuanAn);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        txtTenQuanAn.setText(quanAn.getTenquanan());
        txtDiaChiQuanAn.setText(quanAn.getDiachi());
    }

    private double getChamDiem() {
        int idChecked = rdgChamDiem.getCheckedRadioButtonId();
        double diemCham;
        switch (idChecked) {
            case R.id.rd1:
                diemCham = 1;
                break;
            case R.id.rd2:
                diemCham = 2;
                break;
            case R.id.rd3:
                diemCham = 3;
                break;
            case R.id.rd4:
                diemCham = 4;
                break;
            case R.id.rd5:
                diemCham = 5;
                break;
            case R.id.rd6:
                diemCham = 6;
                break;
            case R.id.rd7:
                diemCham = 7;
                break;
            case R.id.rd8:
                diemCham = 8;
                break;
            case R.id.rd9:
                diemCham = 9;
                break;
            case R.id.rd10:
                diemCham = 10;
                break;
            default:
                diemCham = 0;
                break;
        }
        return diemCham;
    }

    private boolean validate() {
        if (getChamDiem() == 0) {
            Toast.makeText(getApplication(), "Bạn chưa chấm điểm cho quán ăn!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (String.valueOf(edNoiDungBinhLuan.getText()).trim().length() == 0) {
            Toast.makeText(getApplication(), "Bạn chưa nhập nội dung bình luận!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
