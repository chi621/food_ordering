package com.project.hcmuaf.foodordering.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.project.hcmuaf.foodordering.R;
import com.project.hcmuaf.foodordering.adapters.AdapterViewPagerTrangChu;
import com.project.hcmuaf.foodordering.entity.User;
import com.project.hcmuaf.foodordering.model.UserModel;

public class TrangChuActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, RadioGroup.OnCheckedChangeListener {

    public static final String USER_LOGIN = "userLogin";
    private ViewPager viewPagerChangChu;
    private RadioGroup grdFoodOrdering;
    private RadioButton rdFood, rdOrdering;
    private ImageView themQuanAn, imvLogout;
    private SharedPreferences sharedPreferences;
    private TextView txtHoTenDangNhap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_trangchu);

        viewPagerChangChu = findViewById(R.id.vpTrangChu);
        grdFoodOrdering = findViewById(R.id.grdFoodOrdering);
        grdFoodOrdering.setOnCheckedChangeListener(this);
        rdFood = findViewById(R.id.rdFood);
        rdOrdering = findViewById(R.id.rdOrdering);
        themQuanAn = findViewById(R.id.themQuanAn);
        txtHoTenDangNhap = findViewById(R.id.txtHoTenDangNhap);
        imvLogout = findViewById(R.id.imvLogout);

        AdapterViewPagerTrangChu adapterViewPagerTrangChu = new AdapterViewPagerTrangChu(getSupportFragmentManager());
        viewPagerChangChu.setAdapter(adapterViewPagerTrangChu);
        viewPagerChangChu.addOnPageChangeListener(this);

        sharedPreferences = getSharedPreferences(LoginActivity.USER_LOGIN_ID, Context.MODE_PRIVATE);
        txtHoTenDangNhap.setText(sharedPreferences.getString(LoginActivity.HO_TEN_USER_LOGIN, ""));
        if (!"2".equals(sharedPreferences.getString(LoginActivity.TYPE_USER_LOGIN, ""))) {
            themQuanAn.setVisibility(View.GONE);
        } else {
            themQuanAn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent iTrangChu = new Intent(TrangChuActivity.this, ThemQuanAnActivity.class);
                    startActivity(iTrangChu);
                }
            });
        }

        imvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                LoginManager.getInstance().logOut();
                Intent iTrangChu = new Intent(TrangChuActivity.this, LoginActivity.class);
                startActivity(iTrangChu);
            }
        });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                rdFood.setChecked(true);
                break;
            case 1:
                rdOrdering.setChecked(true);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.rdFood:
                viewPagerChangChu.setCurrentItem(0);
                break;
            case R.id.rdOrdering:
                viewPagerChangChu.setCurrentItem(1);
                break;
        }
    }
}
