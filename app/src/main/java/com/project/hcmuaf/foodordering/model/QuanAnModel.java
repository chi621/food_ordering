package com.project.hcmuaf.foodordering.model;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.project.hcmuaf.foodordering.database.QuanAnDatabase;
import com.project.hcmuaf.foodordering.entity.Comment;
import com.project.hcmuaf.foodordering.entity.QuanAn;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HC on 11/12/2018.
 */

public class QuanAnModel {
    private String KEY_DATABASE = "quanans";
    private String HINH_ANH = "hinhanhquanans";
    private DatabaseReference nodeRoot;
    private UserModel userModel;

    QuanAn quanAnRtn;

    public QuanAnModel() {
        userModel = new UserModel();
        this.nodeRoot = FirebaseDatabase.getInstance().getReference();
    }

    public void getDanhSachQuanAn(final QuanAnDatabase quanAnDatabase) {
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    DataSnapshot snapshotQuanAn = dataSnapshot.child(KEY_DATABASE);
                    // get DS quan an
                    for (DataSnapshot snapshot : snapshotQuanAn.getChildren()) {
                        QuanAn quanAn = snapshot.getValue(QuanAn.class);
                        quanAn.setMaquanan(snapshot.getKey());

                        // get hinh anh cho quan an
                        DataSnapshot snapshot1HinhAnh = dataSnapshot.child(HINH_ANH).child(snapshot.getKey());
                        List<String> lstHinhAnh = new ArrayList<>();
                        for (DataSnapshot snapshotlTemp : snapshot1HinhAnh.getChildren()) {
                            lstHinhAnh.add(snapshotlTemp.getValue(String.class));
                        }
                        quanAn.setHinhanhQuanAn(lstHinhAnh);

                        // get danh sach binh luan
                        DataSnapshot dataSnapsComment = dataSnapshot.child("binhluans").child(snapshot.getKey());
                        List<Comment> lstComment = new ArrayList<>();
                        for (DataSnapshot snapshotlTemp : dataSnapsComment.getChildren()) {
                            Comment comment = snapshotlTemp.getValue(Comment.class);
                            comment.setUser(userModel.getUserByUid(dataSnapshot, comment.getMauser()));
                            lstComment.add(comment);
                        }
                        quanAn.setLstComments(lstComment);

                        quanAnDatabase.getDanhSachQuanAn(quanAn);
                    }
                } catch (Exception ex) {
                    Log.e("getDanhSachQuanAn: ", ex.getMessage());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };
        nodeRoot.addListenerForSingleValueEvent(valueEventListener);
    }

    public QuanAn getQuanAnByMaQuanAn(String maquan) {
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    DataSnapshot snapshotQuanAn = dataSnapshot.child(KEY_DATABASE);
                    // get DS quan an
                    for (DataSnapshot snapshot : snapshotQuanAn.getChildren()) {
                        if (snapshot.getKey().equals(maquan)) {
                            quanAnRtn = snapshot.getValue(QuanAn.class);
                            quanAnRtn.setMaquanan(snapshot.getKey());

                            // get hinh anh cho quan an
                            DataSnapshot snapshot1HinhAnh = dataSnapshot.child(HINH_ANH).child(snapshot.getKey());
                            List<String> lstHinhAnh = new ArrayList<>();
                            for (DataSnapshot snapshotlTemp : snapshot1HinhAnh.getChildren()) {
                                lstHinhAnh.add(snapshotlTemp.getValue(String.class));
                            }
                            quanAnRtn.setHinhanhQuanAn(lstHinhAnh);

                            // get danh sach binh luan
                            DataSnapshot dataSnapsComment = dataSnapshot.child("binhluans").child(snapshot.getKey());
                            List<Comment> lstComment = new ArrayList<>();
                            for (DataSnapshot snapshotlTemp : dataSnapsComment.getChildren()) {
                                Comment comment = snapshotlTemp.getValue(Comment.class);
                                comment.setUser(userModel.getUserByUid(dataSnapshot, comment.getMauser()));
                                lstComment.add(comment);
                            }
                            quanAnRtn.setLstComments(lstComment);
                            break;
                        }
                    }

                } catch (Exception ex) {
                    Log.e("getQuanAnByMaQuanAn: ", ex.getMessage());
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };
        nodeRoot.addListenerForSingleValueEvent(valueEventListener);
        return quanAnRtn;
    };
}
