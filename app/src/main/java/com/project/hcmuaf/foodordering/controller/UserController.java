package com.project.hcmuaf.foodordering.controller;

import com.project.hcmuaf.foodordering.entity.User;
import com.project.hcmuaf.foodordering.model.UserModel;

/**
 * Created by HC on 1/3/2019.
 */

public class UserController {
    UserModel userModel;

    public UserController() {
        userModel = new UserModel();
    }

    public void dangKyThanhVien(User user) {
        userModel.saveUser(user);
    }

}
