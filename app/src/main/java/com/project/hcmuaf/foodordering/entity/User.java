package com.project.hcmuaf.foodordering.entity;

import java.io.Serializable;

/**
 * Created by HC on 11/3/2018.
 */
public class User implements Serializable {
    private String hoten;
    private String hinhAnh;
    private String email;
    private String sdt;
    private String type;
    private String uid;

    public User() {
    }

    public User(String uid, String hoten, String email, String sdt, String hinhAnh, String type) {
        this.uid = uid;
        this.hoten = hoten;
        this.email = email;
        this.sdt = sdt;
        this.type = type;
        this.hinhAnh = hinhAnh;
    }

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    public String getHinhAnh() {
        return hinhAnh;
    }

    public void setHinhAnh(String hinhAnh) {
        this.hinhAnh = hinhAnh;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String mail) {
        this.email = mail;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
