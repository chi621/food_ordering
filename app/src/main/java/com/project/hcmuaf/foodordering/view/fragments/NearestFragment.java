package com.project.hcmuaf.foodordering.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.hcmuaf.foodordering.R;

/**
 * Created by HC on 11/11/2018.
 */

public class NearestFragment extends android.support.v4.app.Fragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_nearest, container, false);
        return view;
    }
}