package com.project.hcmuaf.foodordering.database;

import com.project.hcmuaf.foodordering.entity.QuanAn;

import java.util.List;

/**
 * Created by HC on 11/13/2018.
 */

public interface QuanAnDatabase {

    void getDanhSachQuanAn(QuanAn quanAn);
}
