package com.project.hcmuaf.foodordering.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by HC on 11/12/2018.
 */

public class QuanAn implements Serializable{
    private String maquanan;
    private String tenquanan;
    private String videogioithieu;
    private List<String> tienich;
    private List<String> hinhanhQuanAn;
    private long luotthich;
    private String giodongcua;
    private String giomocua;
    private List<Comment> lstComments;
    private String diachi;
    private double latitude;
    private double longitude;

    public QuanAn() {
    }

    public String getMaquanan() {
        return maquanan;
    }

    public void setMaquanan(String maquanan) {
        this.maquanan = maquanan;
    }

    public String getTenquanan() {
        return tenquanan;
    }

    public void setTenquanan(String tenquanan) {
        this.tenquanan = tenquanan;
    }

    public String getVideogioithieu() {
        return videogioithieu;
    }

    public void setVideogioithieu(String videogioithieu) {
        this.videogioithieu = videogioithieu;
    }

    public List<String> getTienich() {
        return tienich;
    }

    public void setTienich(List<String> tienich) {
        this.tienich = tienich;
    }

    public String getGiodongcua() {
        return giodongcua;
    }

    public void setGiodongcua(String giodongcua) {
        this.giodongcua = giodongcua;
    }

    public String getGiomocua() {
        return giomocua;
    }

    public void setGiomocua(String giomocua) {
        this.giomocua = giomocua;
    }

    public long getLuotthich() {
        return luotthich;
    }

    public void setLuotthich(long luotthich) {
        this.luotthich = luotthich;
    }

    public List<String> getHinhanhQuanAn() {
        return hinhanhQuanAn;
    }

    public void setHinhanhQuanAn(List<String> hinhanhQuanAn) {
        this.hinhanhQuanAn = hinhanhQuanAn;
    }

    public List<Comment> getLstComments() {
        return lstComments;
    }

    public void setLstComments(List<Comment> lstComments) {
        this.lstComments = lstComments;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
