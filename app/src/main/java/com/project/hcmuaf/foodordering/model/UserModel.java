package com.project.hcmuaf.foodordering.model;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.project.hcmuaf.foodordering.entity.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HC on 1/3/2019.
 */

public class UserModel implements ValueEventListener{
    String KEY_DATABASE = "thanhviens";
    DatabaseReference databaseNodeThanhVien;
    List<User> users = new ArrayList<>();

    public UserModel() {
        databaseNodeThanhVien = FirebaseDatabase.getInstance().getReference().child(KEY_DATABASE);
        databaseNodeThanhVien.addValueEventListener(this);
    }

    public void saveUser(User user) {
        try {
            databaseNodeThanhVien.child(user.getUid()).setValue(user);
        } catch (Exception ex) {
            Log.e("saveUser: ", ex.getMessage());
        }
    }

    public User getUserByUid(DataSnapshot dataSnapshot, String uid) {
        User user = null;
        try {
            user = dataSnapshot.child(KEY_DATABASE).child(uid).getValue(User.class);
        } catch (Exception ex) {
            Log.e("getUserByUid: ", ex.getMessage());
        }
        return user;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        users = new ArrayList<>();
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            users.add(snapshot.getValue(User.class));
        }
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    public User getUserByUid(String uid) {
        User user = null;
        try {
            if (!users.isEmpty()) {
                for (User user1 : users) {
                    if(user1.getUid().equals(uid)) {
                        user = user1;
                        break;
                    }
                }
            }

        } catch (Exception ex) {
            Log.e("getUserByUid: ", ex.getMessage());
        }
        return user;
    }

}
