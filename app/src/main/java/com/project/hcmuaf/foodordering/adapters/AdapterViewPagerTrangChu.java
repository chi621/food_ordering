package com.project.hcmuaf.foodordering.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.project.hcmuaf.foodordering.view.fragments.FoodFragment;
import com.project.hcmuaf.foodordering.view.fragments.OrderingFragment;

/**
 * Created by HC on 11/11/2018.
 */

public class AdapterViewPagerTrangChu extends FragmentStatePagerAdapter {

    private FoodFragment foodFragment;
    private OrderingFragment orderingFragment;

    public AdapterViewPagerTrangChu(FragmentManager fm) {
        super(fm);
        foodFragment = new FoodFragment();
        orderingFragment = new OrderingFragment();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return  foodFragment;
            case 1:
                return  orderingFragment;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return 2;
    }
}

