package com.project.hcmuaf.foodordering.controller;

import android.content.Context;
import android.view.View;

import com.project.hcmuaf.foodordering.database.QuanAnDatabase;
import com.project.hcmuaf.foodordering.entity.Comment;
import com.project.hcmuaf.foodordering.entity.QuanAn;
import com.project.hcmuaf.foodordering.model.CommentModel;
import com.project.hcmuaf.foodordering.model.QuanAnModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HC on 1/6/2019.
 */

public class CommentController {
    private CommentModel commentModel;
    private QuanAnModel quanAnModel;
    
    public CommentController() {
        commentModel = new CommentModel();
        quanAnModel = new QuanAnModel();
    }

    public void addComment(String maQuanAn, Comment comment) {
        commentModel.saveComment(maQuanAn, comment);
    }

    public QuanAn getQuanAnByMaQuan(String maQuanAn) {
        return quanAnModel.getQuanAnByMaQuanAn(maQuanAn);
    };
}
